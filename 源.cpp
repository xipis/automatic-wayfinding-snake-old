#pragma warning(disable:6031)
#pragma warning(disable:4996)
#pragma warning(disable:6385)
#pragma comment(lib,"user32.lib")
#include<stdio.h>
#include<stdlib.h>
#include<windows.h>
#include<conio.h>
#include<math.h>
#include<time.h>
#include<queue>
#define GetKey(X) (GetAsyncKeyState(X) & 0x8000)
#define Width 70
#define Height 36
#define Rate 0.95
int FAST = 1;
int LIM = 1, StartSpeed = 10;
const int R = 0, L = 1, U = 2, D = 3;
const int Head = -3, Body = -4, Target = -5;
const int move[4][2] = { {1,0},{-1,0},{0,-1},{0,1} };
char com[50];
int map[Height][Width] = { 0 };
int order[Height * Width], orderCnt = 0;
int backWater = 0;
bool Human = 0;
typedef struct Node {
	int x;
	int y;
}node;
node snakes[Width * Height];
node tsnakes[Width * Height];
int Len = 0, Dir = R, T = 0, tDir = R, Dot = 0;
bool END = 0, STOP = 0;
int interval = StartSpeed, score = 0, Vscore = 10;
node dotpos = {-1,-1};
void setColor(unsigned short ForeColor, unsigned short BackGroundColor);
void Hide(void);
void gotoxy(int x, int y);
void init(void);
void moveSnake(void);
void update(int dir);
void dot(void);
bool checkDeath(void);
void autoDecide(void);
bool findPath(bool all,node snakes[],int Tx,int Ty);
bool buildPath(int Tx,int Ty);
void wander(void);
int main(void)//■//2 Bytes
{
	sprintf(com, "mode con cols=%d lines=%d", Width * 2, Height);
	system(com);
	system("title Snake");
	Hide();
	srand(time(NULL));
	
	while (true) {
		init();
		gotoxy(Width / 2 - 8, Height / 2), printf("Press Y to Control, N to Free");
		int ch;
		while (ch = getch(), ch != 'y' && ch != 'n');
		Human = ch == 'y' ? 1 : 0;
		system("cls");
		if (Human)
			LIM = 10, interval = StartSpeed = 50, FAST = 0;

		moveSnake();//
		sprintf(com, "title Snake #GameOver# Press R to restart");system(com);
		setbuf(stdin, NULL);
		while (getch() != 'r');
		system("cls");
	}

	char s[2];
	fgets(s, sizeof(s), stdin);
	return 0;
}
void setColor(unsigned short ForeColor, unsigned short BackGroundColor)
{
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);//获取当前窗口句柄
	SetConsoleTextAttribute(handle, ForeColor + BackGroundColor * 0x10);//设置颜色
}
void Hide(void)
{
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_CURSOR_INFO CursorInfo;
	GetConsoleCursorInfo(handle, &CursorInfo);
	CursorInfo.bVisible = false;
	SetConsoleCursorInfo(handle, &CursorInfo);
}
void gotoxy(int x, int y)
{
	HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD pos;
	pos.X = 2 * x;
	pos.Y = y;
	SetConsoleCursorPosition(hOut, pos);
}
void init(void)
{
	FAST = 1;
	LIM = 1, StartSpeed = 10;
	Len = 0, Dir = R, T = 0, tDir = R, Dot = 0;
	END = 0, STOP = 0;
	interval = StartSpeed, score = 0, Vscore = 10;
	backWater = 0;
	dotpos.x = -1, dotpos.y = -1;
	snakes[0].x = 4; snakes[0].y = 1; snakes[1].x = 3; snakes[1].y = 1; snakes[2].x = 2; snakes[2].y = 1; Len = 3;
}
void moveSnake(void)//Auto!!!!!!!!!!!!!!!!!
{
	sprintf(com, "title Snake [score: %d ; gap: %d ms]", score, interval);
	system(com);
	dot();
	gotoxy(snakes[0].x, snakes[0].y), setColor(1,0),printf("■"), setColor(7, 0);
	for (int i = 1; i < Len; i++)
		gotoxy(snakes[i].x, snakes[i].y), printf("■");
	while (!END)
	{
		if (GetKey(VK_SPACE))
		{
			Sleep(100);
			while (!GetKey(VK_SPACE));
			Sleep(50);
		}
		if (GetKey(VK_RIGHT) && tDir!=L)
			Dir = R;
		else if (GetKey(VK_LEFT) && tDir != R)
			Dir = L;
		else if (GetKey(VK_UP) && tDir != D)
			Dir = U;
		else if (GetKey(VK_DOWN) && tDir != U)
			Dir = D;
		if (T >= 3) {
			if (!Human)autoDecide();
			T = 0,update(Dir);
		}
		if (!Dot)dot();
		if(!FAST)Sleep(interval);
		T++;
	}
}
void update(int dir)
{
	tDir = dir;
	int tx = snakes[Len - 1].x, ty = snakes[Len - 1].y;
	for (int i = Len - 1; i >= 1; i--)
		snakes[i].x = snakes[i - 1].x, snakes[i].y = snakes[i - 1].y;
	switch (dir) {
	case R:
		snakes[0].x += 1;
		break;
	case L:
		snakes[0].x -= 1;
		break;
	case U:
		snakes[0].y -= 1;
		break;
	case D:
		snakes[0].y += 1;
		break;
	}
	if (checkDeath())
	{
		END = 1;
		return;
	}
	if (dotpos.x == snakes[0].x && dotpos.y == snakes[0].y)
	{
		gotoxy(snakes[0].x, snakes[0].y), setColor(1, 0), printf("■"), setColor(7, 0);
		gotoxy(snakes[1].x, snakes[1].y), printf("■");
		Dot = 0;
		Len++;
		snakes[Len - 1].x = tx, snakes[Len - 1].y = ty;
		score += Vscore;
		Vscore += 10;
		interval = interval > LIM ? interval * Rate : interval;
		sprintf(com, "title Snake [score: %d ; gap: %d ms]", score, interval);
		system(com);
		return;
	}
	gotoxy(tx, ty), printf("  ");//2
	gotoxy(snakes[0].x, snakes[0].y), setColor(1, 0), printf("■"), setColor(7, 0);
	gotoxy(snakes[1].x, snakes[1].y), printf("■");
}
void dot(void)
{
	int x, y, flag;
	do {
		x = rand() % Width, y = rand() % Height;
		flag = 0;
		for(int i=0;i<Len;i++)
			if (snakes[i].x == x && snakes[i].y == y)
			{
				flag = 1;
				break;
			}
	} while (flag);
	Dot = 1;
	dotpos.x = x, dotpos.y = y;
	setColor(4,0),gotoxy(x, y), printf("■"), setColor(7, 0);
}
bool checkDeath(void)
{
	if (snakes[0].x < 0 || snakes[0].x >= Width || snakes[0].y < 0 || snakes[0].y >= Height)
		return 1;
	for (int i = 1; i < Len; i++)
		if (snakes[i].x == snakes[0].x && snakes[i].y == snakes[0].y)
			return 1;
	return 0;
}
void autoDecide(void)
{
	if (orderCnt>0)
	{
		Dir = order[--orderCnt];
		return;
	}

	int Find = findPath(0,snakes,dotpos.x,dotpos.y);

	//for (int y = 0; y < Height; y++)
	//	for (int x = 0; x < Width; x++)
	//		if (map[y][x] > 0 && !(x==dotpos.x && y==dotpos.y))
	//			gotoxy(x, y), printf("%d", map[y][x]),Sleep(1);

	if (!Find)
	{
		backWater++;
		int tFind = findPath(0, snakes, snakes[Len - 1].x, snakes[Len - 1].y);
		if (!tFind)
		{
			//getch();
			wander();
			orderCnt = 0;
			return;
		}

		buildPath(snakes[Len - 1].x, snakes[Len - 1].y);
		Dir = order[--orderCnt];
		orderCnt = 0;////////
		return;
	}

	int bFind=buildPath(dotpos.x,dotpos.y);
	if (!bFind && backWater<=Len*10)
	{
		backWater++;
		//if (backWater > Len)
		//{
		//	Sleep(1000);
		//	wander();
		//	return;
		//}
		findPath(0, snakes, snakes[Len - 1].x, snakes[Len - 1].y);
		buildPath(snakes[Len - 1].x, snakes[Len - 1].y);
	}
	else
		backWater = 0;
	Dir = order[--orderCnt];
}
bool findPath(bool all,node snakes[],int Tx,int Ty)
{
	std::queue<node> path;
	memset(map, -1, sizeof(map));
	int step = 1;
	bool Find = 0;
	for (int i = 0; i < Len; i++)
		map[snakes[i].y][snakes[i].x] = Body;
	map[Ty][Tx] = Target;
	map[snakes[0].y][snakes[0].x] = Head;
	path.push(snakes[0]);
	while (!path.empty() && !Find)
	{
		int list[4],i;
		memset(list, -1, sizeof(list));
		while(true)
		{
			int flag = 0;
			for (int i = 0; i < 4; i++)
				if (list[i] != i)
					flag = 1;
			if (!flag)break;
			while ((list[i = rand() % 4] != -1));
			list[i] = i;
			int x = path.front().x, y = path.front().y;
			x += move[i][0], y += move[i][1];
			if (x < 0 || x >= Width || y < 0 || y >= Height)
				continue;
			if (map[y][x] == Target)
			{
				Find = 1;
				map[Ty][Tx] = step;
				if(!all)break;
			}
			if (map[y][x] != -1)
				continue;
			map[y][x] = step;
			path.push({ x, y });
		}
		path.pop();
		if (!path.empty())
			step = map[path.front().y][path.front().x] + 1;
	}
	return Find;
}
bool buildPath(int Tx,int Ty)
{
	int cnt = 0;
	int x = Tx, y = Ty;
	bool Back = 0;
	memset(order, -1, sizeof(order));
	orderCnt = 0;
	while (!Back)
	{
		for (int i = 0; i < 4; i++)
		{
			int tx = x, ty = y;
			tx += move[i][0], ty += move[i][1];
			if (tx < 0 || tx >= Width || ty < 0 || ty >= Height)
				continue;
			if (map[ty][tx] == Head)
			{
				if (Len+1 - cnt > 0)
				{
					tsnakes[cnt].x = x;
					tsnakes[cnt].y = y;
					cnt++;
				}

				Back = 1;
				if (i == R || i == U)
					order[orderCnt++] = i + 1;
				else
					order[orderCnt++] = i - 1;
				break;
			}
			if (map[ty][tx] == map[y][x] - 1)
			{
				if (Len+1 - cnt > 0)
				{
					tsnakes[cnt].x = x;
					tsnakes[cnt].y = y;
					cnt++;
				}

				x = tx, y = ty;
				//gotoxy(x, y), printf(".");
				if (i == R || i == U)
					order[orderCnt++] = i + 1;
				else
					order[orderCnt++] = i - 1;
				break;
			}
		}
	}
	int res = Len - cnt;
	for (int i = 0; i < res + 1; i++, cnt++)//but 吃完后，蛇会变长
	{
		tsnakes[cnt].x = snakes[i].x;
		tsnakes[cnt].y = snakes[i].y;
	}
	int tFind = findPath(0, tsnakes, tsnakes[Len].x, tsnakes[Len].y);
	return tFind;
}
void wander(void)
{
	int i;
	for (i = 0; i < 4; i++)//R = 0, L = 1, U = 2, D = 3
	{
		if ((Dir == R && i == L) || (Dir == L && i == R) || (Dir == U && i == D) || (Dir == D && i == U))
			continue;
		int flag = 0, tx = snakes[0].x, ty = snakes[0].y;
		switch (i) {
		case R:
			tx = snakes[0].x + 1;
			break;
		case L:
			tx = snakes[0].x - 1;
			break;
		case U:
			ty = snakes[0].y - 1;
			break;
		case D:
			ty = snakes[0].y + 1;
			break;
		}
		if (tx < 0 || tx >= Width || ty < 0 || ty >= Height)
			continue;
		for (int j = 1; j < Len; j++)
			if (snakes[j].x == tx && snakes[j].y == ty)
			{
				flag = 1;
				break;
			}
		if (flag)continue;
		Dir = i;
		break;
	}
}